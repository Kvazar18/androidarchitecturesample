package com.lab.atselkovskiy.githubcomponents.domain

interface UseCase<ARG, RESULT> {
    fun execute(argument: ARG) : RESULT
}