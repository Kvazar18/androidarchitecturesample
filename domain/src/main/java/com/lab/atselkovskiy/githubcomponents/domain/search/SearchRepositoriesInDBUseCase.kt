package com.lab.atselkovskiy.githubcomponents.domain.search

import com.lab.atselkovskiy.data.cache.dao.RepositoriesDAO
import com.lab.atselkovskiy.data.cache.entity.RepositoryEntity
import com.lab.atselkovskiy.githubcomponents.domain.UseCase
import io.reactivex.Flowable

class SearchRepositoriesInDBUseCase(val reposoriesDAO : RepositoriesDAO )
    : UseCase<String, Flowable<List<RepositoryEntity>>> {
    override fun execute(argument: String): Flowable<List<RepositoryEntity>> {
        return reposoriesDAO.searchRepository(argument)
    }
}
