package com.lab.atselkovskiy.data.cache

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.lab.atselkovskiy.data.cache.dao.RepositoriesDAO
import com.lab.atselkovskiy.data.cache.entity.RepositoryEntity

/**
 * Created by a.tselkovskiy on 3/7/18.
 */
@Database(entities = [RepositoryEntity::class], version = 3)
@TypeConverters(DBTypeConverters::class)
abstract class GithubDB : RoomDatabase() {

    abstract fun repositoriesDAO(): RepositoriesDAO

    companion object {
        val NAME = "githubcomponents.db"
    }

}