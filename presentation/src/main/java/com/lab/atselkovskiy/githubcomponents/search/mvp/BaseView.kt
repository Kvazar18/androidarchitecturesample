package com.lab.atselkovskiy.githubcomponents.search.mvp

import com.lab.atselkovskiy.githubcomponents.base.model.ErrorModel

interface BaseView {
    fun onError(error: ErrorModel)
}