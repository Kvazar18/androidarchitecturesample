package com.lab.atselkovskiy.githubcomponents.di.base

import com.lab.atselkovskiy.githubcomponents.di.search.SearchRepositoryModule
import com.lab.atselkovskiy.githubcomponents.search.mvp.SearchRepositoryMVPFragment
import com.lab.atselkovskiy.githubcomponents.search.mvvm.SearchRepositoryMVVMFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by a.tselkovskiy on 3/9/18.
 */

@Module
interface FragmentsModule {

    @ContributesAndroidInjector(modules = [SearchRepositoryModule::class])
    fun buildSearchMVVMFragment(): SearchRepositoryMVVMFragment

    @ContributesAndroidInjector(modules = [SearchRepositoryModule::class])
    fun buildSearchMVPFragment(): SearchRepositoryMVPFragment
}