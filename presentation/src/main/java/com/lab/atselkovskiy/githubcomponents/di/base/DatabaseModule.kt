package com.lab.atselkovskiy.githubcomponents.di.base

import android.app.Application
import androidx.room.Room
import com.lab.atselkovskiy.data.cache.GithubDB
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {
    @Provides
    @Singleton
    fun provideDataBase(application: Application): GithubDB {
        return Room.databaseBuilder(application, GithubDB::class.java, GithubDB.NAME)
                .fallbackToDestructiveMigration()
                .build()
    }

}