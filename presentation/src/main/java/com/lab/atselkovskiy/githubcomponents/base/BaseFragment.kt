package com.lab.atselkovskiy.githubcomponents.base

import android.content.Context
import androidx.fragment.app.Fragment

import dagger.android.support.AndroidSupportInjection

/**
 * Created by a.tselkovskiy on 3/16/18.
 */

open class BaseFragment : Fragment() {

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }
}
