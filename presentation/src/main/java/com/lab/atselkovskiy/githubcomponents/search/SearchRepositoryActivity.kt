package com.lab.atselkovskiy.githubcomponents.search

import android.os.Bundle

import com.lab.atselkovskiy.githubcomponents.R
import com.lab.atselkovskiy.githubcomponents.base.BaseActivity

/**
 * Created by a.tselkovskiy on 3/16/18.
 */

class SearchRepositoryActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_repository)
    }
}