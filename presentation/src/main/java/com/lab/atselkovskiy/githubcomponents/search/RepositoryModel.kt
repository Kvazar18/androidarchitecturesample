package com.lab.atselkovskiy.githubcomponents.search

/**
 * Created by a.tselkovskiy on 3/16/18.
 */

data class RepositoryModel(val id: Long,
                           val name: String?,
                           val avatar: String?,
                           val starsCount: Int,
                           val watchersCount: Int,
                           val forksCount: Int,
                           val language: String?)