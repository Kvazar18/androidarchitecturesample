package com.lab.atselkovskiy.githubcomponents.di.base

import com.lab.atselkovskiy.githubcomponents.search.SearchRepositoryActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by a.tselkovskiy on 3/9/18.
 */
@Module
interface ActivitiesModule {
    @ContributesAndroidInjector(modules = [FragmentsModule::class])
    fun contributeHomeActivity(): SearchRepositoryActivity
}