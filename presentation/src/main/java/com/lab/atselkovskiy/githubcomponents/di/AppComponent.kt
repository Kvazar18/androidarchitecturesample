package com.lab.atselkovskiy.githubcomponents.di

import android.app.Application
import com.lab.atselkovskiy.githubcomponents.GithubApp
import com.lab.atselkovskiy.githubcomponents.di.base.ActivitiesModule
import com.lab.atselkovskiy.githubcomponents.di.base.DatabaseModule
import com.lab.atselkovskiy.githubcomponents.di.base.NetworkModule
import com.lab.atselkovskiy.githubcomponents.di.base.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

/**
 * Created by a.tselkovskiy on 3/7/18.
 */
@Singleton
@Component(modules = [
    AndroidInjectionModule::class,
    NetworkModule::class,
    DatabaseModule::class,
    ActivitiesModule::class,
    ViewModelModule::class])
interface AppComponent {
    fun inject(app: GithubApp)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }
}
