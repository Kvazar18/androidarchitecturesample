package com.lab.atselkovskiy.githubcomponents.di.search

import com.lab.atselkovskiy.data.api.APIService
import com.lab.atselkovskiy.data.cache.GithubDB
import com.lab.atselkovskiy.githubcomponents.domain.search.FetchAndSaveRepositoriesUseCase
import com.lab.atselkovskiy.githubcomponents.domain.search.SearchRepositoriesInDBUseCase
import dagger.Module
import dagger.Provides

@Module
class SearchRepositoryModule {

    @Provides
    fun provideSearchRepositoriesInDBUseCase(db: GithubDB): SearchRepositoriesInDBUseCase {
        return SearchRepositoriesInDBUseCase(db.repositoriesDAO())
    }

    @Provides
    fun provideFetchAndSaveRepositoriesUseCase(db: GithubDB, api: APIService): FetchAndSaveRepositoriesUseCase{
        return FetchAndSaveRepositoriesUseCase(api, db.repositoriesDAO())
    }
}